const Koa = require('koa');
const static = require('koa-static')
const path = require("path")
const app = new Koa()
const STATIC_PATH = path.join(__dirname, '/')
app.use(static(STATIC_PATH)); 
app.listen(3087);