const path = require('path')
// const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

const UglifyPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: {
    main: './src/js/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'base.js'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,  
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
        // use: ['style-loader', 'css-loader', 'sass-loader'] 
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/'
            },
          },
        ] 
      }
    ]
  },
  plugins: [
    new UglifyPlugin(),
    new MiniCssExtractPlugin(),
    new CopyWebpackPlugin(
      {
        patterns: [
          {
            from: path.resolve(__dirname, 'src/static'), 
            to: path.resolve(__dirname, 'dist/static')
          },
          {
            from: path.resolve(__dirname, 'src/index.html'), 
            to: path.resolve(__dirname, 'dist/index.html')
          }
        ]
      }
    )
    // new HtmlWebpackPlugin({
    //   filename: 'index.html',
    //   template: 'assets/index.html'
    // })
    
  ]
}