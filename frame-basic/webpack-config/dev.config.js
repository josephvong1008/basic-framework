const path = require('path')
// const HtmlWebpackPlugin = require('html-webpack-plugin')
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// const UglifyPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: {
    main: './src/js/index.js'
  },
  output: {
    path: path.resolve(__dirname, '../', 'dist'), // '../dist',
    filename: 'base.js'
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.scss$/,  
        // use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
        use: ['style-loader', 'css-loader', 'sass-loader'] 
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/'
            },
          },
        ] 
      }
    ]
  },
  resolve: {
    alias: {
      '$ui': path.resolve(__dirname, '@xdh/my/ui/lib')
    }
  },
  plugins: [
    // new MiniCssExtractPlugin(),
    // new HtmlWebpackPlugin({
    //   filename: 'index.html',
    //   template: 'assets/index.html'
    // })
    
  ]
}