const headerTemplate = `
<div id="Header">
  <div class="title">
  <em class="el-icon-eleme"></em>
  {{title}}
  </div>
  <div class="sub-title">{{subTitle}}</div>
</div>
`
Vue.component('x-header', {
  props: {
    title: {
      type: String,
      default: 'title'
    },
    subTitle: {
      type: String,
      default: 'sib-title'
    }
  },
  template: headerTemplate,
  data: function() {
    return {
       
    }
  },
  methods: {}
})