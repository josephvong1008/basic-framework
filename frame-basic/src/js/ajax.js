const axios = require('axios')
const source = axios.CancelToken.source()
const staticConfig = {
  /**
   * 请求头
   */
  headers: {
    'Content-Type': 'application/json;charset=UTF-8'
  },
  /**
   * 超时时间
   */
  timeout: 0,
  /**
   * 携带验证
   */
  withCredentials: true,
  /**
   * 响应数据类型
   */
  responseType: 'json',
  /**
   * 请求内容长度
   */
  maxContentLength: -1
}
// console.log(axios)
function createInstance() {
  return axios.create({
    ...staticConfig,
    cancelToken: source.token
  })
}
const response = function(response) {
  /*
  if (response.data.status === 401 || response.data.status === 403) {
    // this.logout()
    setTimeout(() => {
      window.location.reload()
    }, 500)
  } else {
    // 在这里实现对请求前的处理
    return response
  } 
  */
  console.log('now response')
  return response
}

// 请求拦截函数，axios=true 有效
const request = function(config) { 
  console.log('now requet')
  // config.headers.authorization = 'token' 
  // 在这里实现对请求前的处理
  return config
}
const service = createInstance()
service.interceptors.request.use(request)
service.interceptors.response.use(response)
window.$ajax = service